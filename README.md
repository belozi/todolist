# README

This is a Ruby on Rails based To Do List application.

To run this app, cd into the todolist folder in a terminal or cmd window, and run "rails server."

After that, either right-click on the https://localhost:3000 address and open the link,

or type the address into your browswer. The app should open at the overview/index page. 

Alternatively, I have hosted this app on heroku @: https://rbl-todolist.herokuapp.com/

Enjoy!

