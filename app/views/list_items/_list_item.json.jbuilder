json.extract! list_item, :id, :item, :details, :deadline, :completed, :created_at, :updated_at
json.url list_item_url(list_item, format: :json)
