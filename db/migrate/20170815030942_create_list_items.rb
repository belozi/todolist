class CreateListItems < ActiveRecord::Migration[5.1]
  def change
    create_table :list_items do |t|
      t.string :item
      t.text :details
      t.date :deadline
      t.boolean :completed

      t.timestamps
    end
  end
end
