Rails.application.routes.draw do
  resources :list_items
  root 'list_items#index'
end